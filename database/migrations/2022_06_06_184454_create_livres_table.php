<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('livres', function (Blueprint $table) {
            $table->id();
            $table->string('Titre') ;
            $table->string('EtatduLivre') ;
            $table->string('Auteur') ;
            $table->string('Langues') ;
            $table->string('Maisonedition') ;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('livres');
    }
};
