@extends('modele')
<style>
    table {
        font-family: Verdana, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }
    tr:nth-child(even) {
        background-color: #dddddd;
    }
</style>

@section('contents')
    <div>
        @foreach($livre as $l)
            <table>
                <tr>
                    <td>
                      <label> <table><ul><li>{{$l->Titre}},{{$l->Auteur}},{{$l->EtatduLivre}},{{$l->Langues}},{{$l->Maisonedition}}</li></ul></table></label>
                    </td>
                </tr>
            </table>
        @endforeach
    </div>
@endsection

