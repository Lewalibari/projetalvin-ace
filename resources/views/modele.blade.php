<!DOCTYPE html>
<html>

<head>

    <title>

    </title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <style>
        .etat{background-color: red}
        .error{background-color: rgb(250,78,104);}
    </style>
</head>
<body>



@section('menu')
    @guest
        <a href="{{route('login')}}">Login</a>
        <a href="{{route('register')}}">Register</a>
    @endguest

    @auth
        <a href="{{route('logout')}}">Logout</a>
        <p>{{Auth::user()}} {{Auth::id()}}</p>
        <a href="{{route('home')}}">Home</a><br>
    @endauth

@show
@section('etat')
    @if($errors->any())
        <div class="error">
            <ul>
                @foreach($errors->all() as $errors)
                    <li>{{$errors}}</li>
                @endforeach
            </ul>
        </div>
    @endif
@show
@yield('contents')
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>

