<?php

namespace App\Http\Controllers;

use App\Models\Livre;
use Illuminate\Http\Request;

class LivreController extends Controller
{



        public function book(){
        $b = Livre::all() ;
        return view('livresviews.livre',['livre'=>$b]) ;
         }

        public function livre(){
        return view('welcome');
        }

           public function storelivre(Request $request){
           $v=$request->validate([
             'titre'=>'required|string|max:67',
             'etatdulivre'=>'required|string|max:56',
             'auteur'=>'required|string|max:78',
             'langues'=>'required|string|max:67',
             'maisonedition'=>'required|string|max:78',
              ]);
           $livre = new Livre();
           $livre->titre = $v['titre'];
           $livre->etatdulivre =$v['etatdulivre'];
           $livre->auteur = $v['auteur'];
           $livre->langues = $v['langues'];
           $livre->maisonedition = $v['maisonedition'];
           $livre->save() ;
           return redirect()->route('word');
      }
}
