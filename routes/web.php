<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::get('/livre',[\App\Http\Controllers\LivreController::class,'livre'])->middleware(['auth'])->name('livre') ;
Route::post('/livre',[\App\Http\Controllers\LivreController::class,'storelivre'])->middleware(['auth']);
Route::get('/livres',[\App\Http\Controllers\LivreController::class,'book'])->name('boock');

require __DIR__.'/auth.php';
